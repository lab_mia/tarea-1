# Tarea No. 1
### Laboratorio - Manejo e Implementación de Archivos.

## Instrucciones
- Fork the repo. 
- Crear una rama. El nombre de la rama es: CARNET. No símbolos
ni antes ni después, solamente su número de carnet será el nombre de la rama.
- Editar el README. Deberán agregar una entrada en el área
de entregas (ver más abajo) y agregar su nombre y número de carnet.
- Crear un Merge Request. Es importante esta parte, luego de 
crear su rama y editar el README, deberán crear un pull request
para que yo pueda tomar sus cambios en cuenta.

## Notas
- El link al repositorio será publicado en Uedi.
- No habrá entregable en Uedi. El pull request es suficiente.
- Envíar Pull Request a más tardar Sábado 31 de Julio a media noche.

## Entregas
- Renato Flores, 201709244.
- Kevin Lopez, 201901016.
- Miguel Guirola, 201700772.
- Adrian Molina, 201903850
- Brian Morales, 201318564.
- Kevin Calderon, 201902714
- Marvin Rodriguez, 201709450.
- Elder Andrade, 201700858
- Byron Par, 201701078.
- Sergio Felipe Zapeta, 200715274.
- Julio Wu, 201906180.
- Brandon Yax, 201800534.
- Jeser Collado, 201213458.
- Herberth Avila, 201504464.
- Jorge Castañeda, 201809938.
- Steven Jocol, 201602938.
- Jorge Pérez, 201900810.
- Kenny Rodas, 201602894.
- Alvaro Garcia, 199817948.
- Brando Muñoz, 201700890.
- Bryan Méndez, 201801528.
- Maynor Piló, 201531166.
- Jairo Ramírez, 201800712.
- Douglas Soch, 201807032.
- Daniel Santos, 201325512.
- Pedro Martin, 201700656.
- Kenneth López, 201906570.
- Cesar Chinchilla, 201612132.
- Jose Santos, 201643762.
- Byron Hernández, 201806840.
- David Díaz, 201807420.
- Erick Alvarado, 201800546.
- Daniel Alexander Cano, 201807162.
- Omar Vides, 201709146.
- Jonathan Alexander Alvarado Fernández, 201903004.
- José Rafael Solis Franco, 201612344.
- Ana Belen Contreras, 201901604.
- Oscar Augusot Perez Tzunun 201213498.
- Joel Estuardo Rodríguez  Santos, 201115018.
- Luis Enrique López Urbina,  201212818.
- Benaventi Bernal Fuentes Roldan, 201021212.
- Karen Lisbeth Morales Marroquin, 201908316.
- Edwin Mauricio Lopez Mazariegos, 199213640.
- Mádelyn Zuseth Pérez Rosales, 201612558.
- Angel Manuel Elias Catú, 201403982
- Carlos Javier Martinez Polanco, 201709282
- Mádelyn Zuseth Pérez Rosales, 201612558.
- Pablo Oliva, 201700898
- Julio Orellana, 201908120.
